/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        "primary-color": "#3374FF",
        "secondary-color": "#FFB133",
        "tertiary-color": "#8233FF ",
      },
      mytheme: {
        primary: "#00bbfd",

        secondary: "#00e4ff",

        accent: "#0080ff",

        neutral: "#041e0b",

        "base-100": "#fffbff",

        info: "#007dff",

        success: "#008e00",

        warning: "#8e6600",

        error: "#b6143d",
      },
    },
  },
  plugins: [require("daisyui")],
};
