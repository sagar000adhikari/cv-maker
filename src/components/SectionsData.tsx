export const sections = [
  {
    id: "experience",
    title: "Experience",
    description: "Description for Experience section",
  },
  {
    id: "skills",
    title: "Skills",
    description: "Description for Skills section",
  },
  {
    id: "projects",
    title: "Projects",
    description: "Description for Projects section",
  },
  {
    id: "strengths",
    title: "Strengths",
    description: "Description for Strengths section",
  },
  {
    id: "experience",
    title: "Experience",
    description: "Description for Experience section",
  },
  {
    id: "languages",
    title: "Languages",
    description: "Description for Languages section",
  },
];
