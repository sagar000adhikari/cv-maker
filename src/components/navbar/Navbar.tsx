const NavbarComponent = () => {
  return (
    <div className="navbar  flex justify-between ">
      <div className="flex-1">
        <a className="flex items-center text-xl">CV MAKER</a>
      </div>
      <div className=" flex gap-8">
        <ul tabIndex={0} className=" flex gap-8">
          <li>
            <a className=" cursor-pointer hover:border-b-2 hover:border-blue-400">
              Item 1
            </a>
          </li>
          <li>
            <a className=" cursor-pointer hover:border-b-2 hover:border-blue-400">
              Parent
            </a>
          </li>
          <li>
            <a className=" cursor-pointer hover:border-b-2 hover:border-tertiary-color">
              Item 3
            </a>
          </li>
        </ul>
        <div className="flex gap-2">
          <div className="form-control">
            <button className="btn bg-primary-color text-white">Upgrade</button>
          </div>
          <div className="dropdown dropdown-end">
            <div
              tabIndex={0}
              role="button"
              className="btn flex gap-2 btn-ghost btn-circle avatar">
              <div className="w-10 rounded-full">
                <img
                  alt="Tailwind CSS Navbar component"
                  src="https://daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg"
                />
              </div>
            </div>
            <ul
              tabIndex={0}
              className="mt-3 z-[1] p-2 shadow menu menu-sm dropdown-content bg-base-100 rounded-box w-52">
              <li>
                <a className="justify-between">
                  Plans
                  <span className="badge">New</span>
                </a>
              </li>
              <li>
                <a>Account</a>
              </li>
              <li>
                <a className=" justify-between">
                  Language
                  <span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6">
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M12 21a9.004 9.004 0 0 0 8.716-6.747M12 21a9.004 9.004 0 0 1-8.716-6.747M12 21c2.485 0 4.5-4.03 4.5-9S14.485 3 12 3m0 18c-2.485 0-4.5-4.03-4.5-9S9.515 3 12 3m0 0a8.997 8.997 0 0 1 7.843 4.582M12 3a8.997 8.997 0 0 0-7.843 4.582m15.686 0A11.953 11.953 0 0 1 12 10.5c-2.998 0-5.74-1.1-7.843-2.918m15.686 0A8.959 8.959 0 0 1 21 12c0 .778-.099 1.533-.284 2.253m0 0A17.919 17.919 0 0 1 12 16.5c-3.162 0-6.133-.815-8.716-2.247m0 0A9.015 9.015 0 0 1 3 12c0-1.605.42-3.113 1.157-4.418"
                      />
                    </svg>
                  </span>
                </a>
              </li>
              <li>
                <a>Logout</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NavbarComponent;
