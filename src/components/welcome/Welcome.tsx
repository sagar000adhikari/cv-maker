import { useState } from "react";
import { Link, Outlet, useNavigate } from "react-router-dom";

const Welcome = () => {
  const [activeTab, setActiveTab] = useState("allDocuments");
  const navigate = useNavigate();

  const handleTabClick = (tab: string) => {
    setActiveTab(tab);
    navigate(`/${tab}`);
  };

  return (
    <div className="py-10 px-20 items-center bg-gray-50">
      <div className="flex justify-between">
        <h1 className="text-2xl">Welcome back, USER! You have 0 document</h1>
        <span>
          <button className="btn bg-primary-color text-white">Upgrade</button>
        </span>
      </div>
      <div role="tablist" className="tabs tabs-bordered">
        <Link
          role="tab"
          className={`tab ${activeTab === "allDocuments" ? "tab-active" : ""}`}
          onClick={() => handleTabClick("allDocuments")}
          to="/allDocuments">
          All Documents
        </Link>
        <Link
          role="tab"
          className={`tab ${activeTab === "resumes" ? "tab-active" : ""}`}
          onClick={() => handleTabClick("resumes")}
          to="/resumes">
          Resumes (1)
        </Link>
        <Link
          role="tab"
          className={`tab ${activeTab === "coverLetters" ? "tab-active" : ""}`}
          onClick={() => handleTabClick("coverLetters")}
          to="/coverLetters">
          Cover letters (0)
        </Link>
      </div>
      <Outlet />
    </div>
  );
};

export default Welcome;
