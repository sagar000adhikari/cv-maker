// import AddSectionComponent from "../../sidebarComponents/addSection/AddSectionComponent";

// const ResumeSidebar = ({ onAddSection }: any) => {
//   const sections = [
//     {
//       id: "experience",
//       title: "Experience",
//       description: "Description for Experience section",
//     },
//     {
//       id: "skills",
//       title: "Skills",
//       description: "Description for Skills section",
//     },
//     {
//       id: "education",
//       title: "Education",
//       description: "Description for Education section",
//     },
//     {
//       id: "projects",
//       title: "Projects",
//       description: "Description for Projects section",
//     },
//     {
//       id: "strengths",
//       title: "Strengths",
//       description: "Description for Strengths section",
//     },
//     {
//       id: "languages",
//       title: "Languages",
//       description: "Description for Languages section",
//     },
//     {
//       id: "achievements",
//       title: "Achievements",
//       description: "Description for Achievements section",
//     },
//   ];
//   const handleAddSection = (sectionId: any) => {
//     onAddSection(sectionId);
//   };
//   return (
//     <ul className="menu bg-white px-5 w-56 gap-4">
//       {sections.map((section) => (
//         <li key={section.id} className="cursor-pointer hover:bg-gray-300">
//           <AddSectionComponent
//             key={section.id}
//             section={section}
//             onClick={handleAddSection}
//           />
//         </li>
//       ))}
//       <li className=" cursor-pointer hover:bg-gray-300 p-3">Rearrange</li>
//       <li className=" cursor-pointer hover:bg-gray-300 p-3">Templates</li>
//       <hr />
//       <li className=" cursor-pointer hover:bg-gray-300 p-3">Download</li>
//       <li className=" cursor-pointer hover:bg-gray-300 p-3">Share</li>
//     </ul>
//   );
// };

// export default ResumeSidebar;
import AddSectionComponent from "../../sidebarComponents/addSection/AddSectionComponent";

const ResumeSidebar = ({ onAddSection }: any) => {
  const sections = [
    {
      id: "experience",
      title: "Experience",
      description: "Description for Experience section",
    },
    {
      id: "skills",
      title: "Skills",
      description: "Description for Skills section",
    },
    {
      id: "education",
      title: "Education",
      description: "Description for Education section",
    },
    {
      id: "projects",
      title: "Projects",
      description: "Description for Projects section",
    },
    {
      id: "strengths",
      title: "Strengths",
      description: "Description for Strengths section",
    },
    {
      id: "languages",
      title: "Languages",
      description: "Description for Languages section",
    },
    {
      id: "achievements",
      title: "Achievements",
      description: "Description for Achievements section",
    },
  ];

  const handleAddSection = (sectionId: any) => {
    onAddSection(sectionId);
  };

  return (
    <ul className="menu bg-white px-5 w-56 gap-4">
      <li className=" cursor-pointer hover:bg-gray-300">
        <AddSectionComponent sections={sections} onClick={handleAddSection} />
      </li>
      <li className=" cursor-pointer hover:bg-gray-300 p-3">Rearrange</li>
      <li className=" cursor-pointer hover:bg-gray-300 p-3">Templates</li>
      <hr />
      <li className=" cursor-pointer hover:bg-gray-300 p-3">Download</li>
      <li className=" cursor-pointer hover:bg-gray-300 p-3">Share</li>
    </ul>
  );
};

export default ResumeSidebar;
