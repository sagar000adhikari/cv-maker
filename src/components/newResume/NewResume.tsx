// import SummaryComponent from "../fields/summary/SummaryComponent";
// import EducationComponent from "../fields/education/EducationComponent";
// import ExperienceComponent from "../fields/experience/ExperienceComponent";
// import LanguageComponent from "../fields/languages/LanguageComponent";
// import StrengthsComponent from "../fields/strengths/StrengthsComponent";
// import SkillsComponent from "../fields/skills/SkillsComponent";
// import AchievementsComponent from "../fields/achievements/AchievementsComponent";
// import NameComponent from "../fields/nameField/NameComponent";
// import ResumeSidebar from "./sidebar/ResumeSidebar";
// import { useState } from "react";

// const NewResume = () => {
//   const [showSummary, setShowSummary] = useState(false);
//   const [showEducation, setShowEducation] = useState(false);
//   const [showLanguages, setShowLanguages] = useState(false);
//   const [showStrengths, setShowStrengths] = useState(false);

//   const handleAddSummary = () => {
//     setShowSummary(true);
//   };
//   const handleEducation = () => {
//     setShowEducation(true);
//   };
//   const handleLanguages = () => {
//     setShowLanguages(true);
//   };
//   const handleStrengths = () => {
//     setShowStrengths(true);
//   };
//   return (
//     <div className="flex justify-between bg-white pt-4">
//       <ResumeSidebar onAddSection={handleAddSummary} />
//       <div className="max-w-[70vw] flex flex-col p-20 rounded-sm justify-center bg-white shadow-2xl">
//         <NameComponent />
//         <div className=" grid grid-cols-2 gap-4">
//           <div>
//             {showSummary && (
//               <div>
//                 <SummaryComponent />
//               </div>
//             )}
//             {showEducation && (
//               <div>
//                 <EducationComponent />
//               </div>
//             )}

//             <div>
//               <ExperienceComponent />
//             </div>
//             {showLanguages && (
//               <div>
//                 <LanguageComponent />
//               </div>
//             )}
//           </div>
//           <div>
//             {showStrengths && (
//               <div>
//                 <StrengthsComponent />
//               </div>
//             )}
//             <div>
//               <SkillsComponent />
//             </div>
//             <div>
//               <AchievementsComponent />
//             </div>
//           </div>
//         </div>
//         <div>Footer of the cv</div>
//       </div>
//     </div>
//   );
// };

// export default NewResume;
import { useState } from "react";
import SummaryComponent from "../fields/summary/SummaryComponent";
import EducationComponent from "../fields/education/EducationComponent";
import ExperienceComponent from "../fields/experience/ExperienceComponent";
import LanguageComponent from "../fields/languages/LanguageComponent";
import StrengthsComponent from "../fields/strengths/StrengthsComponent";
import SkillsComponent from "../fields/skills/SkillsComponent";
import AchievementsComponent from "../fields/achievements/AchievementsComponent";
import NameComponent from "../fields/nameField/NameComponent";
import ResumeSidebar from "./sidebar/ResumeSidebar";

const NewResume = () => {
  // const [showSummary, setShowSummary] = useState(false);
  const [showEducation, setShowEducation] = useState(false);
  const [showLanguages, setShowLanguages] = useState(false);
  const [showStrengths, setShowStrengths] = useState(false);
  const [showSkills, setShowSkills] = useState(false);
  const [showExperience, setShowExperience] = useState(false);
  const [showAchievements, setShowAchievements] = useState(false);

  const handleAddSection = (sectionId: any) => {
    switch (sectionId) {
      // case "summary":
      //   setShowSummary(true);
      //   break;
      case "education":
        setShowEducation(true);
        break;
      case "languages":
        setShowLanguages(true);
        break;
      case "strengths":
        setShowStrengths(true);
        break;
      case "skills":
        setShowSkills(true);
        break;
      case "experience":
        setShowExperience(true);
        break;
      case "achievements":
        setShowAchievements(true);
        break;
      default:
        break;
    }
  };

  return (
    <div className="flex justify-between bg-white pt-4 pr-4">
      <ResumeSidebar onAddSection={handleAddSection} />
      <div className="max-w-[70vw] flex flex-col p-20 rounded-sm justify-center bg-white shadow-2xl">
        <NameComponent />
        <div className=" grid grid-cols-2 gap-4">
          <div>
            {/* {showSummary && <SummaryComponent />} */}
            <SummaryComponent />
            {showEducation && <EducationComponent />}
            {showExperience && <ExperienceComponent />}
            {showLanguages && <LanguageComponent />}
          </div>
          <div>
            {showStrengths && <StrengthsComponent />}
            {showSkills && <SkillsComponent />}
            {showAchievements && <AchievementsComponent />}
          </div>
        </div>
        <div>Footer of the cv</div>
      </div>
    </div>
  );
};

export default NewResume;
