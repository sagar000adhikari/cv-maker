const AllDocumentsTab = () => {
  return (
    <div className="grid grid-cols-1 md:grid-cols-1 lg:grid-cols-2 gap-6 py-10  items-center justify-center">
      <a href="/newResume">
        <div className="new-resume rounded-2xl w-auto glass md:flex py-8 px-5 hidden items-center hover:bg-gray-100 cursor-pointer ease-in-out duration-300">
          <div className="p-5 m-6 h-auto border-2 items-center text-center justify-center flex rounded-sm text-white w-96 bg-secondary-color">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6">
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
              />
            </svg>
          </div>
          <div className=" ">
            <h2 className="card-title mb-2">New Resume</h2>
            <p>
              <b>Tip: </b>Did you know that if you tailor your resume to the job
              description, you double your chances to get an interview?
            </p>
          </div>
        </div>
      </a>
      <div className=" rounded-2xl w-auto glass md:flex py-8 px-5 hidden items-center hover:bg-gray-100 cursor-pointer ease-in-out duration-300">
        <div className="p-5 m-6 h- border-2 items-center text-center justify-center flex rounded-sm text-white w-96 bg-secondary-color">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-6 h-6">
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"
            />
          </svg>
        </div>
        <div className=" ">
          <h2 className="card-title mb-2">New Cover Letter</h2>
          <p>
            <b>Tip: </b>Did you know that if you tailor your resume to the job
            description, you double your chances to get an interview?
          </p>
        </div>
      </div>
      <div className=" rounded-2xl w-auto glass md:flex py-8 px-5 hidden items-center hover:bg-gray-100 cursor-pointer ease-in-out duration-300">
        <div className=" pr-5">
          <img src="https://picsum.photos/200/300" alt="resume_img" />
        </div>
        <div className=" w-full flex flex-col ">
          <div className=" flex justify-between align-top">
            <h1 className=" w-full flex-1 flex">RESUME #1</h1>
            <div className="max-w-[100px] text-center items-center align-middle justify-center flex rounded-full">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6">
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M9.568 3H5.25A2.25 2.25 0 0 0 3 5.25v4.318c0 .597.237 1.17.659 1.591l9.581 9.581c.699.699 1.78.872 2.607.33a18.095 18.095 0 0 0 5.223-5.223c.542-.827.369-1.908-.33-2.607L11.16 3.66A2.25 2.25 0 0 0 9.568 3Z"
                />
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M6 6h.008v.008H6V6Z"
                />
              </svg>
              <input
                type="text"
                placeholder="label"
                className=" btn-ghost max-w-[50px] text-center"
              />
            </div>
          </div>
          <div className="flex items-center justify-between gap-2">
            <input
              type="text"
              placeholder="My Resume"
              className="input pl-0 text-left input-ghost grow w-full max-w-xs"
            />
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              className="w-5 h-5">
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L6.832 19.82a4.5 4.5 0 0 1-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 0 1 1.13-1.897L16.863 4.487Zm0 0L19.5 7.125"
              />
            </svg>
          </div>
          <div>
            <p className=" text-[12px] text-gray-400">Edited few seconds ago</p>
          </div>
          <hr className="mt-5" />
          <div>
            <button
              className="btn"
              onClick={() => document.getElementById("my_modal_5").showModal()}>
              open modal
            </button>
            <dialog
              id="my_modal_5"
              className="modal modal-bottom sm:modal-middle">
              <div className="modal-box">
                <h3 className="font-bold text-lg">Hello!</h3>
                <p className="py-4">
                  Press ESC key or click the button below to close
                </p>
                <div className="modal-action">
                  <form method="dialog">
                    {/* if there is a button in form, it will close the modal */}
                    <button className="btn">Close</button>
                  </form>
                </div>
              </div>
            </dialog>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AllDocumentsTab;
