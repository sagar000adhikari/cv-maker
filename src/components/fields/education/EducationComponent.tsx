import React, { useEffect, useRef, useState } from "react";

interface EducationEntry {
  degree: string;
  type: string;
  startYear: number | undefined;
  endYear: string | undefined;
}

const EducationComponent = () => {
  const textareaRefs = useRef<(HTMLTextAreaElement | null)[]>([]);
  const [educationEntries, setEducationEntries] = useState<EducationEntry[]>([
    {
      degree: "",
      type: "school",
      startYear: undefined,
      endYear: "Present",
    },
  ]);

  const addEducationEntry = () => {
    setEducationEntries([
      ...educationEntries,
      {
        degree: "",
        type: "school",
        startYear: undefined,
        endYear: "Present",
      },
    ]);
  };
  const [activeFieldIndex, setActiveFieldIndex] = useState(null); // Track active field

  const handleTextareaChange = (index: number) => {
    const textarea = textareaRefs.current[index];
    if (textarea) {
      textarea.style.height = "auto";
      textarea.style.height = textarea.scrollHeight + "px";
    }
  };

  const handleRadioChange = (index: number, value: string) => {
    const newEntries = [...educationEntries];
    newEntries[index].type = value;
    setEducationEntries(newEntries);
  };

  const handleStartYearChange = (
    index: number,
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const value = parseInt(event.target.value);
    const newEntries = [...educationEntries];
    newEntries[index].startYear = value;
    setEducationEntries(newEntries);
  };

  const handleEndYearChange = (
    index: number,
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const value = event.target.value;
    const newEntries = [...educationEntries];
    newEntries[index].endYear = value;
    setEducationEntries(newEntries);
  };

  const handleFieldClick = (index: any) => {
    setActiveFieldIndex(index); // Set active field on click
  };

  const handleDeleteEntry = (indexToDelete: number) => {
    setEducationEntries((prevEntries) => {
      return prevEntries.filter((_, index) => index !== indexToDelete);
    });
    setActiveFieldIndex(null);
  };

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (
        activeFieldIndex !== null &&
        !event.target.closest(".flex.flex-wrap")
      ) {
        setActiveFieldIndex(null); // Reset if clicked outside and field is active
      }
    };

    document.addEventListener("click", handleClickOutside);

    return () => document.removeEventListener("click", handleClickOutside); // Cleanup
  }, [activeFieldIndex]);

  return (
    <div className="mb-7 text-black">
      <h1 className="border-b-2 flex justify-between border-gray-200 text-2xl font-semibold">
        EDUCATION{" "}
        <span onClick={addEducationEntry} className="cursor-pointer">
          +
        </span>
      </h1>
      <div className="flex flex-wrap">
        {educationEntries.map((entry, index) => (
          <div
            key={index}
            className="mb-4 relative"
            onClick={() => handleFieldClick(index)}>
            <textarea
              ref={(ref) => (textareaRefs.current[index] = ref)}
              className="textarea border-none textarea-ghost max-h-2 bg-white max-w-5xl w-full font-semibold flex-grow overflow-hidden !pl-0 !pt-0"
              placeholder="Degree & Field of Education"
              onChange={() => handleTextareaChange(index)}></textarea>
            <div className="flex gap-4 mb-1">
              <div
                className={`radio-container flex gap-2 items-center ${
                  entry.type === "school" ? "active" : ""
                }`}>
                <input
                  type="radio"
                  name={`radio-${index}`}
                  className="radio checked:bg-blue-400 w-3 h-3 checked:shadow-none"
                  checked={entry.type === "school"}
                  onChange={() => handleRadioChange(index, "school")}
                />
                <p
                  className={`${
                    entry.type === "school" ? "text-blue-400" : "text-gray-200"
                  }`}>
                  School
                </p>
              </div>
              <div
                className={`radio-container flex bg-white gap-2 items-center ${
                  entry.type === "university" ? "active" : ""
                }`}>
                <input
                  type="radio"
                  name={`radio-${index}`}
                  className="radio checked:bg-blue-400 w-3 h-3 bg-white checked:shadow-none"
                  checked={entry.type === "university"}
                  onChange={() => handleRadioChange(index, "university")}
                />
                <p
                  className={`${
                    entry.type === "university"
                      ? "text-blue-400"
                      : "text-gray-200"
                  }`}>
                  University
                </p>
              </div>
            </div>
            <div className="flex gap-4">
              <input
                type="number"
                placeholder="Start Year"
                max={new Date().getFullYear()}
                value={entry.startYear || ""}
                onChange={(e) => handleStartYearChange(index, e)}
                className="max-w-[84px] text-center font-semibold bg-white"
              />
              <span>-</span>
              <input
                type="text"
                placeholder="End Year"
                max={new Date().getFullYear()}
                value={entry.endYear || ""}
                onChange={(e) => handleEndYearChange(index, e)}
                className="text-center font-semibold bg-white w-32"
              />
            </div>
            {activeFieldIndex === index && (
              <button
                className="absolute text-[10px] -right-2 top-0 mt-1 mr-1 text-gray-500 hover:text-red-500"
                onClick={() => handleDeleteEntry(index)}>
                &#10006;
              </button>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default EducationComponent;
