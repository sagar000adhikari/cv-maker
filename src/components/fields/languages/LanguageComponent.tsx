import { useEffect, useState } from "react";

const LanguageComponent = () => {
  const [languages, setLanguages] = useState([{ name: "", rating: 1 }]);
  const [activeFieldIndex, setActiveFieldIndex] = useState(null); // Track active field

  const addLanguage = () => {
    setLanguages([...languages, { name: "", rating: 1 }]);
  };
  const handleLanguageChange = (index: any, name: any) => {
    const updatedLanguages = [...languages];
    updatedLanguages[index].name = name;
    setLanguages(updatedLanguages);
  };

  const handleDeleteLanguage = (index: any) => {
    setLanguages((prevLanguages) => {
      const newLanguages = [...prevLanguages];
      newLanguages.splice(index, 1);
      return newLanguages;
    });
    setActiveFieldIndex(null); // Reset active field after deletion
  };

  const handleFieldClick = (index: any) => {
    setActiveFieldIndex(index); // Set active field on click
  };

  const handleRatingChange = (index: any, rating: any) => {
    const updatedLanguages = [...languages];
    updatedLanguages[index].rating = rating;
    setLanguages(updatedLanguages);
  };

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (
        activeFieldIndex !== null &&
        !event.target.closest(".flex.flex-wrap")
      ) {
        setActiveFieldIndex(null); // Reset if clicked outside and field is active
      }
    };

    document.addEventListener("click", handleClickOutside);

    return () => document.removeEventListener("click", handleClickOutside); // Cleanup
  }, [activeFieldIndex]);

  return (
    <div className="mb-7 text-black">
      <h1 className="border-b-2 mb-2 border-gray-200 text-2xl font-semibold flex justify-between">
        LANGUAGES{" "}
        <span className="cursor-pointer" onClick={addLanguage}>
          +
        </span>
      </h1>
      <div className=" flex flex-wrap gap-4">
        {languages.map((language, index) => (
          <div key={index}>
            <div className="relative" onClick={() => handleFieldClick(index)}>
              <input
                className="flex-grow bg-white text-sm w-auto flex input-ghost !focus:border-none !border-none items-center !font-bold"
                type="text"
                name="text"
                placeholder="Language"
                value={language.name}
                onChange={(e) => handleLanguageChange(index, e.target.value)}
              />
              <div className="flex gap-3 items-center">
                <p className="flex flex-grow">
                  {language.rating === 1
                    ? "Beginner"
                    : language.rating === 2
                    ? "Intermediate"
                    : language.rating === 3
                    ? "Advanced"
                    : ""}
                </p>
                <div className="rating rating-sm flex gap-3">
                  {[1, 2, 3].map((rating) => (
                    <input
                      key={rating}
                      type="radio"
                      name={`rating-${index}`}
                      className="mask bg-primary-color !w-3 !h-3  mask-circle"
                      checked={language.rating === rating}
                      onChange={() => handleRatingChange(index, rating)}
                    />
                  ))}
                </div>
                {activeFieldIndex === index && (
                  <button
                    className="absolute text-[10px] -right-2 top-0 mt-1 mr-1 text-gray-500 hover:text-red-500"
                    onClick={() => handleDeleteLanguage(index)}>
                    &#10006;
                  </button>
                )}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default LanguageComponent;
