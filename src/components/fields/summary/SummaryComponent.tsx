import { useRef } from "react";

const SummaryComponent = () => {
  const textareaRef = useRef<HTMLTextAreaElement | null>(null);

  const handleTextareaChange = () => {
    const textarea = textareaRef.current;
    if (textarea) {
      textarea.style.height = "auto";
      textarea.style.height = textarea.scrollHeight + "px";
    }
  };

  return (
    <div className=" text-black mb-[6px]">
      <h1 className="border-b-2 border-gray-200 text-2xl font-semibold">
        SUMMARY
      </h1>
      <textarea
        ref={textareaRef}
        className="textarea border-none bg-white textarea-ghost max-w-5xl w-full font-semibold flex-grow overflow-hidden !pl-0 !pt-0"
        placeholder="Degree & Field of Education"
        onChange={handleTextareaChange}></textarea>
    </div>
  );
};

export default SummaryComponent;
