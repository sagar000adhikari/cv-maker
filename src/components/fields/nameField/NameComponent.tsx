import { useState } from "react";

const NameComponent = () => {
  const [userData, setUserData] = useState({
    name: "",
    jobPosition: "",
    phone: "",
    email: "",
    linkedIn: "",
    location: "",
  });

  const handleChange = (event: any) => {
    const { name, value } = event.target;
    setUserData((prevData) => ({ ...prevData, [name]: value }));
  };
  const extractLinkedInName = (url: any) => {
    const regex = /https:\/\/www.linkedin.com\/in\/(.+)/;
    const match = url.match(regex);
    return match ? match[1] : ""; // Return the matched part after "https://www.linkedin.com/in/"
  };
  return (
    <div className="flex flex-col gap-4 mb-8">
      <div className="text-4xl tracking-wider pb-1 font-bold">
        <input
          className="input input-ghost border-none text-black bg-white p-0 text-4xl uppercase tracking-wider font-bold"
          type="text"
          name="name"
          value={userData.name}
          onChange={handleChange}
          placeholder="YOUR NAME"
        />
      </div>
      <div className=" pb-1 font-semibold">
        <input
          className=" text-primary-color bg-white font-semibold"
          type="text"
          name="jobPosition"
          value={userData.jobPosition}
          onChange={handleChange}
          placeholder="Job Position"
        />
      </div>
      <div className="flex gap-4 flex-wrap">
        <p className="flex gap-2 text-sm items-center font-semibold">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="#60A5FA"
            className="w-4 h-4">
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M2.25 6.75c0 8.284 6.716 15 15 15h2.25a2.25 2.25 0 0 0 2.25-2.25v-1.372c0-.516-.351-.966-.852-1.091l-4.423-1.106c-.44-.11-.902.055-1.173.417l-.97 1.293c-.282.376-.769.542-1.21.38a12.035 12.035 0 0 1-7.143-7.143c-.162-.441.004-.928.38-1.21l1.293-.97c.363-.271.527-.734.417-1.173L6.963 3.102a1.125 1.125 0 0 0-1.091-.852H4.5A2.25 2.25 0 0 0 2.25 4.5v2.25Z"
            />
          </svg>
          <input
            className="flex-grow text-sm input-ghost text-black bg-white items-center font-semibold"
            type="tel"
            name="phone"
            value={userData.phone}
            onChange={handleChange}
            placeholder="Phone Number"
          />
        </p>
        <p className=" flex flex-grow gap-2 bg-white text-sm items-center font-semibold">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="#60A5FA"
            className="w-4 h-4">
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M16.5 12a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0Zm0 0c0 1.657 1.007 3 2.25 3S21 13.657 21 12a9 9 0 1 0-2.636 6.364M16.5 12V8.25"
            />
          </svg>
          <input
            className="flex-grow text-sm w-auto text-black bg-white flex input-ghost items-center font-semibold"
            type="text"
            name="email"
            value={userData.email}
            onChange={handleChange}
            placeholder="Email"
          />
        </p>
        <p className=" flex gap-2 text-sm bg-white  items-center font-semibold">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="#60A5FA"
            className="w-4 h-4">
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M13.19 8.688a4.5 4.5 0 0 1 1.242 7.244l-4.5 4.5a4.5 4.5 0 0 1-6.364-6.364l1.757-1.757m13.35-.622 1.757-1.757a4.5 4.5 0 0 0-6.364-6.364l-4.5 4.5a4.5 4.5 0 0 0 1.242 7.244"
            />
          </svg>

          <input
            className="flex-grow bg-white text-sm input-ghost text-black items-center font-semibold"
            type="text"
            name="linkedIn"
            value={extractLinkedInName(userData.linkedIn)}
            onChange={handleChange}
            placeholder="LinkedIn"
          />
        </p>
        <p className=" flex gap-2 bg-white text-sm items-center font-semibold">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="#60A5FA"
            className="w-4 h-4">
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"
            />
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1 1 15 0Z"
            />
          </svg>
          <input
            className="flex-grow bg-white text-sm input-ghost text-black items-center font-semibold"
            type="text"
            name="location"
            value={userData.location}
            onChange={handleChange}
            placeholder="Location"
          />
        </p>
      </div>
    </div>
  );
};

export default NameComponent;
