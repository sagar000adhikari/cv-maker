import { useEffect, useState } from "react";

const StrengthsComponent = () => {
  const [strengths, setStrengths] = useState([{ talent: "", result: "" }]);
  const [activeFieldIndex, setActiveFieldIndex] = useState(null); // Track active field

  const addStrength = () => {
    setStrengths([...strengths, { talent: "", result: "" }]);
  };

  const handleInputChange = (index: number, key: any, value: any) => {
    const newStrengths = [...strengths];
    newStrengths[index][key] = value;
    setStrengths(newStrengths);
  };

  const handleDeleteStrength = (index: any) => {
    setStrengths((prevStrengths) => {
      const newStrengths = [...prevStrengths];
      newStrengths.splice(index, 1);
      return newStrengths;
    });
    setActiveFieldIndex(null); // Reset active field after deletion
  };

  const handleFieldClick = (index: any) => {
    setActiveFieldIndex(index); // Set active field on click
  };

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (
        activeFieldIndex !== null &&
        !event.target.closest(".flex.flex-wrap")
      ) {
        setActiveFieldIndex(null); // Reset if clicked outside and field is active
      }
    };

    document.addEventListener("click", handleClickOutside);

    return () => document.removeEventListener("click", handleClickOutside); // Cleanup
  }, [activeFieldIndex]);

  return (
    <div className="mb-7 text-black">
      <h1 className="uppercase border-b-2 mb-2 border-gray-200 text-2xl font-semibold flex justify-between">
        Strengths{" "}
        <span className="cursor-pointer" onClick={addStrength}>
          +
        </span>
      </h1>
      <div className="flex flex-wrap">
        {strengths.map((strength, index) => (
          <div key={index} className="mb-4">
            <div className="relative" onClick={() => handleFieldClick(index)}>
              <input
                className="flex-grow bg-white text-sm max-w-[500px] w-full flex input-ghost border-none items-center font-bold mb-2"
                type="text"
                placeholder="Your Unique Talent"
                value={strength.talent}
                onChange={(e) =>
                  handleInputChange(index, "talent", e.target.value)
                }
              />
              <input
                className="flex-grow text-sm w-auto flex break-words bg-white input-ghost !focus:border-none !border-none items-center font-semibold"
                type="text"
                placeholder="What did it result in?"
                value={strength.result}
                onChange={(e) =>
                  handleInputChange(index, "result", e.target.value)
                }
              />
              {activeFieldIndex === index && (
                <button
                  className="absolute text-[10px] -right-2 top-0 mt-1 mr-1 text-gray-500 hover:text-red-500"
                  onClick={() => handleDeleteStrength(index)}>
                  &#10006;
                </button>
              )}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default StrengthsComponent;
