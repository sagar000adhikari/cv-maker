import { useEffect, useState } from "react";

interface ExperienceProps {
  title: string;
  companyName: string;
  startYear: number | undefined;
  endYear: string;
}
const ExperienceComponent = () => {
  const [experiences, setExperiences] = useState<ExperienceProps[]>([
    { title: "", companyName: "", startYear: undefined, endYear: "Present" },
  ]);

  const addExperience = () => {
    setExperiences([
      ...experiences,
      { title: "", companyName: "", startYear: undefined, endYear: "Present" },
    ]);
  };
  const [activeFieldIndex, setActiveFieldIndex] = useState(null); // Track active field

  const handleInputChange = (
    index: number,
    key: keyof ExperienceProps,
    value: string | number
  ) => {
    const newExperiences = [...experiences];
    newExperiences[index][key] = value;
    setExperiences(newExperiences);
  };

  const handleFieldClick = (index: any) => {
    setActiveFieldIndex(index); // Set active field on click
  };

  const handleDeleteEntry = (indexToDelete: number) => {
    setExperiences((prevEntries) => {
      return prevEntries.filter((_, index) => index !== indexToDelete);
    });
    setActiveFieldIndex(null);
  };

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (
        activeFieldIndex !== null &&
        !event.target.closest(".flex.flex-wrap")
      ) {
        setActiveFieldIndex(null); // Reset if clicked outside and field is active
      }
    };

    document.addEventListener("click", handleClickOutside);

    return () => document.removeEventListener("click", handleClickOutside); // Cleanup
  }, [activeFieldIndex]);

  return (
    <div className="mb-7 text-black">
      <h1 className="border-b-2 mb-2 border-gray-200 text-2xl font-semibold flex justify-between">
        EXPERIENCE{" "}
        <span className="cursor-pointer" onClick={addExperience}>
          +
        </span>
      </h1>
      <div className=" flex flex-wrap">
        {experiences.map((experience, index) => (
          <div
            key={index}
            className="mb-4 relative"
            onClick={() => handleFieldClick(index)}>
            <input
              className="flex-grow bg-white text-sm w-auto flex input-ghost border-none items-center font-semibold mb-2"
              type="text"
              placeholder="Title"
              value={experience.title}
              onChange={(e) =>
                handleInputChange(index, "title", e.target.value)
              }
            />
            <input
              className="flex-grow text-sm w-auto flex !text-blue-400 placeholder-blue-400 bg-white input-ghost !focus:border-none !border-none items-center font-semibold mb-2"
              type="text"
              placeholder="Company Name"
              value={experience.companyName}
              onChange={(e) =>
                handleInputChange(index, "companyName", e.target.value)
              }
            />
            <div className="flex gap-4">
              <input
                type="number"
                placeholder="Start Year"
                max={new Date().getFullYear()}
                value={experience.startYear || ""}
                onChange={(e) =>
                  handleInputChange(
                    index,
                    "startYear",
                    parseInt(e.target.value)
                  )
                }
                className="max-w-[84px] font-semibold bg-white text-center"
              />
              <span>-</span>
              <input
                type="text"
                placeholder="End Year / Present"
                max={new Date().getFullYear()}
                value={experience.endYear || ""}
                onChange={(e) =>
                  handleInputChange(index, "endYear", e.target.value)
                }
                className="text-center font-semibold bg-white w-32"
              />
            </div>
            {activeFieldIndex === index && (
              <button
                className="absolute text-[10px] -right-2 top-0 mt-1 mr-1 text-gray-500 hover:text-red-500"
                onClick={() => handleDeleteEntry(index)}>
                &#10006;
              </button>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default ExperienceComponent;
