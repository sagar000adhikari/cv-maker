import { useEffect, useState } from "react";

const SkillsComponent = () => {
  const [skills, setSkills] = useState([""]);
  const [activeFieldIndex, setActiveFieldIndex] = useState(null); // Track active field

  const handleAddSkill = () => {
    setSkills([...skills, ""]);
  };

  const handleSkillChange = (index: any, value: any) => {
    const newSkills = [...skills];
    newSkills[index] = value;
    setSkills(newSkills);
  };

  const handleFieldClick = (index: any) => {
    setActiveFieldIndex(index); // Set active field on click
  };

  const handleDeleteSkill = (index: any) => {
    const newSkills = [...skills];
    newSkills.splice(index, 1);
    setSkills(newSkills);
    setActiveFieldIndex(null); // Reset active field after deletion
  };

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (
        activeFieldIndex !== null &&
        !event.target.closest(".flex.flex-wrap")
      ) {
        setActiveFieldIndex(null); // Reset if clicked outside and field is active
      }
    };

    document.addEventListener("click", handleClickOutside);

    return () => document.removeEventListener("click", handleClickOutside); // Cleanup
  }, [activeFieldIndex]);

  return (
    <div className=" mb-7 text-black">
      <h1 className="uppercase flex justify-between border-b-2 mb-2 border-gray-200 text-2xl font-semibold">
        Skills{" "}
        <span onClick={handleAddSkill} className=" cursor-pointer">
          +
        </span>
      </h1>
      <div className=" flex flex-wrap">
        {skills.map((skill, index) => (
          <div
            key={index}
            className="relative mb-2 items-center justify-center align-middle"
            onClick={() => handleFieldClick(index)}>
            <input
              className="flex-grow text-sm text-blue-400 w-auto flex font-semibold bg-white input-ghost !focus:border-none !border-none items-center mb-2"
              type="text"
              name="text"
              placeholder="Tools/Technology"
              value={skill}
              onChange={(e) => handleSkillChange(index, e.target.value)}
            />
            {activeFieldIndex === index && (
              <button
                className="absolute text-[10px] -right-2 top-0 mt-1 mr-1 text-gray-500 hover:text-red-500"
                onClick={() => handleDeleteSkill(index)}>
                &#10006;
              </button>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default SkillsComponent;
