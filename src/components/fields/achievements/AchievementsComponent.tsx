import { useEffect, useState } from "react";

const AchievementsComponent = () => {
  const [achievements, setAchievements] = useState([""]);
  const [activeFieldIndex, setActiveFieldIndex] = useState(null); // Track active field

  const addAchievement = () => {
    setAchievements([...achievements, ""]);
  };

  const deleteAchievement = (index: any) => {
    const newAchievements = [...achievements];
    newAchievements.splice(index, 1);
    setAchievements(newAchievements);
    setActiveFieldIndex(null); // Reset active field after deletion
  };

  const handleAchievementChange = (index: any, value: any) => {
    const newAchievements = [...achievements];
    newAchievements[index] = value;
    setAchievements(newAchievements);
  };

  const handleFieldClick = (index: any) => {
    setActiveFieldIndex(index); // Set active field on click
  };
  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (activeFieldIndex !== null && !event.target.closest(".relative")) {
        setActiveFieldIndex(null); // Reset if clicked outside and field is active
      }
    };

    document.addEventListener("click", handleClickOutside);

    return () => document.removeEventListener("click", handleClickOutside); // Cleanup
  }, [activeFieldIndex]);

  return (
    <div className="mb-7 text-black">
      <h1 className="uppercase border-b-2 mb-2 border-gray-200 text-2xl font-semibold flex justify-between">
        Achievements{" "}
        <span className="cursor-pointer" onClick={addAchievement}>
          +
        </span>
      </h1>
      <div className="flex flex-wrap gap-4">
        {achievements.map((achievement, index) => (
          <div
            key={index}
            className="relative mb-2 items-center justify-center align-middle max-w-[200px] w-full"
            onClick={() => handleFieldClick(index)}>
            <input
              className="flex-grow bg-white text-sm  align-middle  w-full flex input-ghost border-none items-center font-semibold"
              type="text"
              name="text"
              placeholder="What are you most proud of?"
              value={achievement}
              onChange={(e) => handleAchievementChange(index, e.target.value)}
            />
            {activeFieldIndex === index && (
              <button
                className="absolute text-[10px] -right-5 top-0 mt-1 mr-1 text-gray-500 hover:text-red-500"
                onClick={() => deleteAchievement(index)}>
                &#10006;
              </button>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default AchievementsComponent;
