// const sections = [
//   {
//     id: "experience",
//     title: "Experience",
//     description: "Description for Experience section",
//   },
//   {
//     id: "skills",
//     title: "Skills",
//     description: "Description for Skills section",
//   },
//   {
//     id: "projects",
//     title: "Projects",
//     description: "Description for Projects section",
//   },
//   {
//     id: "strengths",
//     title: "Strengths",
//     description: "Description for Strengths section",
//   },
//   {
//     id: "education",
//     title: "Education",
//     description: "Description for Education section",
//   },
//   {
//     id: "languages",
//     title: "Languages",
//     description: "Description for Languages section",
//   },
//   {
//     id: "achievements",
//     title: "Achievements",
//     description: "Description for Achievements section",
//   },
// ];

// const AddSectionComponent = ({ section, onClick }: any) => {
//   return (
//     <div onClick={() => document.getElementById("my_modal_3").showModal()}>
//       <button className="!p-0 !m-0">Add Section</button>
//       <dialog id="my_modal_3" className="modal">
//         <div className="modal-box !bg-white !text-black !max-w-none">
//           <form method="dialog">
//             <button className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2">
//               ✕
//             </button>
//           </form>
//           <h3 className="font-bold text-lg flex justify-center mb-4">
//             Add a New Section
//           </h3>
//           <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
//             {sections.map((section) => (
//               <div
//                 key={section.id}
//                 className="card bg-blue-400 text-primary-content">
//                 <div className="card-body">
//                   <h2 className="card-title flex justify-center">
//                     {section.title}
//                   </h2>
//                   <p className=" flex justify-center">{section.description}</p>
//                   <div className="card-actions justify-center mt-2">
//                     <button
//                       className="bg-primary-color p-3 rounded text-white hover:bg-blue-300"
//                       onClick={() => onClick(section.id)}>
//                       Add Section
//                     </button>
//                   </div>
//                 </div>
//               </div>
//             ))}
//           </div>
//         </div>
//       </dialog>
//     </div>
//   );
// };

// export default AddSectionComponent;

const AddSectionComponent = ({ sections, onClick }: any) => {
  return (
    <div>
      <button onClick={() => document.getElementById("my_modal_3").showModal()}>
        Add a new section
      </button>
      <dialog id="my_modal_3" className="modal">
        <div className="modal-box !bg-white !text-black !max-w-none overflow-y-scroll">
          <form method="dialog">
            <button className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2">
              ✕
            </button>
          </form>
          <h3 className="font-bold text-lg flex justify-center mb-4 overflow">
            Add a New Section
          </h3>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
            {sections.map((section: any) => (
              <div
                key={section.id}
                className="card bg-blue-400 text-primary-content">
                <div className="card-body">
                  <h2 className="card-title flex justify-center">
                    {section.title}
                  </h2>
                  <p className=" flex justify-center">{section.description}</p>
                  <div className="card-actions justify-center mt-2">
                    <button
                      className="bg-primary-color p-3 rounded text-white hover:bg-blue-300"
                      onClick={() => onClick(section.id)}>
                      Add Section
                    </button>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </dialog>
    </div>
  );
};

export default AddSectionComponent;
