import { Outlet, createBrowserRouter, RouterProvider } from "react-router-dom";
import NavbarComponent from "./components/navbar/Navbar";
import Welcome from "./components/welcome/Welcome";
import AllDocumentsTab from "./components/allDocuments/AllDocumentsTab";
import ResumeTab from "./components/resume/ResumeTab";
import CoverLetterTab from "./components/coverLetter/CoverLetterTab";
import NewResume from "./components/newResume/NewResume";

const Layout = () => {
  return (
    <>
      <div className="">
        <div className="flex flex-col">
          <NavbarComponent />
          <div>
            <Outlet />
          </div>
        </div>
      </div>
    </>
  );
};

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "/",
        element: <Welcome />,
        children: [
          { path: "/", element: <AllDocumentsTab /> },
          { path: "allDocuments", element: <AllDocumentsTab /> },
          { path: "resumes", element: <ResumeTab /> },
          { path: "coverLetters", element: <CoverLetterTab /> },
        ],
      },
      {
        path: "/newResume",
        element: <NewResume />,
      },
    ],
  },
]);

const App = () => {
  return (
    <div className="">
      <RouterProvider router={router} />
    </div>
  );
};

export default App;
